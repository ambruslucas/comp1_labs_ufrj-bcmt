from lab2 import discriminante


#1
def valor_absoluto(n):
	"""não funciona com números complexos
	float -> float"""
	if n < 0:
		return float(-n)
	else: 
		return float(n) 


#2
def n_raizes(a,b,c):
	"""informar a, b e c
	int,int,int -> str"""
	if discriminante(a,b,c) > 0:
		return '2 raízes reais'
	elif discriminante(a,b,c) == 0:
		return '1 raíz real'
	else:
		return 'nenhuma raíz real'


#3
def mensagem_repetida(texto, vezes):
	"""informar texto entre aspas e quantas vezes quer repetir
	str,int -> str"""
	return str(texto) * vezes


#4
def data(d,m,a):
	"""informar dia, mês e ano
	int,int,int -> str"""
	return str(d) + '/' + str(m) + '/' + str(a)


#5
def func_pyth(x):
	"""informar um valor de x
	float -> float"""
	if x < 0:
		return float(0) 
	elif x <= 2:
		return float(x) 
	elif x <= 3.5:
		return float(2)
	elif x <= 5:
		return float(3)
	else:
		return float(x**2 - 10 * x + 28)


#6a
def desconto_inss(salario_bruto):
	"""informar salario bruto
	float -> float"""
	if salario_bruto <= 2000:
		return float(0.06 * salario_bruto)
	elif salario_bruto <= 3000:
		return float(0.08 * salario_bruto)
	else:
		return float(0.10 * salario_bruto)


#6b
def desconto_ir(salario_bruto):
	"""informar salario bruto
	float -> float"""
	if salario_bruto <= 2500:
		return float(0.11 * salario_bruto)
	elif salario_bruto <= 5000:
		return float(0.15 * salario_bruto)
	else:
		return float(0.22 * salario_bruto)


#6c
def desconto_total(salario_bruto):
	return float(salario_bruto) - (desconto_inss(salario_bruto) + desconto_inss(salario_bruto))

	


	


