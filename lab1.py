#1
def calc_area(lado1, lado2):
	"""informar os dois lados do retângulo"""
	return lado1 * lado2


#2
def area_sup_cubo(a):
	"""informar a aresta do cubo"""
	return 6 * a ** 2


#3
def area_coroa(r2, r1):
	"""inserir o maior raio seguido do menor raio"""
	return ((r2**2) * 3.14) - ((r1**2) * 3.14)


#4
def media(valor1, valor2):
	"""informar os dois valores"""
	return (valor1 + valor2) / 2 


#5
def achar_ordenada(a, b, c, x):
	"""informar a, b, c e x"""
	return a * x**2 + b * x + c


#6
def media_ponderada(peso_valor1, peso_valor2, valor1, valor2):
	"""chamar a função informando na ordem os pesos dos valores 1 e 2 seguidos dos valores 1 e 2"""
	return (valor1 * peso_valor1 + valor2 * peso_valor2) / (peso_valor1 + peso_valor2)


#7
def erro_aproximação(q, n):
	"""informar a razão e o números de termos a serem somados"""
	return  1 / (1 - q) - (1 - q**n) / (1 - q)                               


#8
def gorjeta(total_conta):
	"""informar o total da conta"""
	return 0.15 * total_conta


#9
def gorjeta_local(total_conta, porcentagem):
	"""informar porcentagem da gorjeta em decimais, ex: 10, 20, 25"""
	return porcentagem/100 * total_conta


#10
def func_juros(saldo_inicial, numero_meses, taxa_juros):
	"""informar saldo, meses e a taxa de juros em decimais, ex: 10, 20, 35"""

	return saldo_inicial * (1 + taxa_juros * numero_meses)


#11
def distancia_correnteza_abaixo(v_correnteza, v_barco, l_rio):
	"""informar a velocidade da correnteza e a velocidade do barco em m/s e a largura do rio em m"""	

	return v_correnteza * (l_rio/v_barco)


